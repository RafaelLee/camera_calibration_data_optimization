CC=gcc
CXX=g++

# https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html

# C flags
# with all warnings, with extra warnings
CFLAGS := -Wall -Wextra -std=c99 -DDEBUG -g -fsanitize=address -O0
# C preprocess flags
CPPFLAGS :=

# Extra flags to give to the C++ compiler.
CXXFLAGS := -Wall -Wextra -std=c++14 -DDEBUG -g -fsanitize=address -O0

# -Wl,(XX) means send xx to linker
# -Wl,--gc-sections means send --gc-sections to linker
LDFLAGS := -Wl,--gc-sections -g -fsanitize=address -O0

# the first target in makefile is the default target
# which means
# `make` is the same with `make all`

# target: dependencies
# the next line of target starts with tab


TOP := $(shell pwd)/
SRC := .
OBJ := obj
INC := -I$(TOP) -Iinclude -I/usr/include/eigen3
LIB := -lglog -lgflags -lceres
all_cxx_source_files := $(sort $(shell find $(SRC) -name "*.cc"))
all_c_source_files := $(sort $(shell find $(SRC) -name "*.c"))
all_o_files := $(all_cxx_source_files:.cc=.o)
all_o_files += $(all_c_source_files:.c=.o)
all_o_files := $(addprefix $(OBJ)/,$(all_o_files))
all_d_files := $(all_o_files:.o=.d)


TARGET := main
# LD is not using implicit rule
$(TARGET): $(all_o_files) $(OBJ)
	# $(CC) $(LDFLAGS) $^ -o $(TARGET)
	$(CXX) $(LDFLAGS) $(all_o_files) -o $(TARGET) $(LIB)

# $@ target
# $< first prerequisite
# $(@D) the directory part of $@
# $^ The names of all the prerequisites, with spaces between them
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html

# http://make.mad-scientist.net/papers/advanced-auto-dependency-generation/
# https://www.gnu.org/software/make/manual/html_node/Automatic-Prerequisites.html
-include $(all_d_files)

# where to put OBJ is a question, sometimes compile will fail due to sequence error
.PHONY: $(OBJ)
$(OBJ):
	mkdir -p $(OBJ)


$(OBJ)/%.d: %.cc
	mkdir -p $(OBJ)
	set -e; rm -f $@; \
	$(CXX) -MM $(CXXFLAGS) $(INC) $< > $@.$$$$; \
	sed 's|\($*\)\.o[ :]*|\1.o $@ : |g' < $@.$$$$ > $@; \
	rm -f $@.$$$$


$(OBJ)/%.d: %.c
	mkdir -p $(OBJ)
	set -e; rm -f $@; \
	$(CC) -MM $(CFLAGS) $(INC) $< > $@.$$$$; \
	sed 's|\($*\)\.o[ :]*|\1.o $@ : |g' < $@.$$$$ > $@; \
	rm -f $@.$$$$


$(OBJ)/%.o: %.cc # $(OBJ)
	mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INC) -c $^ -o $@


$(OBJ)/%.o: %.c # $(OBJ)
	mkdir -p $(@D)
	$(CC) $(CFLAGS) $(INC) -c $^ -o $@


%.o:
	echo "please use target obj/$@ instead"
	exit 1

%.d:
	echo "please use target obj/$@ instead"
	exit 1


# A phony target is one that is not really the name of a file
.PHONY: clean
clean:
	rm -f $(TARGET)
	rm -rf $(OBJ)
	rm -f *.o
	rm -f *.d
	rm -f GTAGS GRTAGS GPATH
