// https://ceres-solver.googlesource.com/ceres-solver/+/master/examples/curve_fitting.cc


// Ceres Solver - A fast non-linear least squares minimizer
// Copyright 2015 Google Inc. All rights reserved.
// http://ceres-solver.org/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// * Neither the name of Google Inc. nor the names of its contributors may be
//   used to endorse or promote products derived from this software without
//   specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Author: Rafael Lee
#include "ceres/ceres.h"
#include "ceres/rotation.h"
#include "glog/logging.h"
using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;

#include "Eigen/Core"
#include <sys/time.h>
static int g_cnt = 0;
// from ceres-solver/examples/bal_problem.cc
template<typename T>
void FscanfOrDie (FILE *fptr, const char *format, T *value)
{
  int num_scanned = fscanf (fptr, format, value);
  if (num_scanned != 1)
  {
    fprintf (stderr, "Invalid UW data file.");
  }
}


// side offect
void read_calibration_to_array (FILE *fptr, double *p, ssize_t length)
{
  for (int i = 0; i < length; i++)
  {
    FscanfOrDie (fptr, "%lf", p + i);
  }
}


// template <typename T>
// void print_matrix (T matrix)
void print_matrix (Eigen::MatrixXd matrix)
{
  int r = matrix.rows();
  int c = matrix.cols();

  std::cout << "[";
  for (int i = 0; i < r; ++i)
  {
    std::cout << "[";
    for (int j = 0; j < c; ++j)
    {
      std::cout << matrix (i, j);
      if (j != c - 1)
      {
        std::cout  << ", ";
      }
    }

    if (i != r - 1)
    {
      std::cout << "]," << std::endl;
    }
    else
    {
      std::cout << "]]" << std::endl;
    }
  }
}


// template <typename T>
// void pm (T matrix)
void pm (Eigen::MatrixXd matrix)
{
  print_matrix (matrix);
}


template <typename T>
//                      3x3               1x3
// void rotation_matrix (T *const r, const T *const rot)
void rotation_matrix (double *r, const T *const rot)
{
  T theta = sqrt (rot[0] * rot[0] + rot[1] * rot[1] + rot[2] * rot[2]);
  T nx = rot[0] / theta;
  T ny = rot[1] / theta;
  T nz = rot[2] / theta;

  // T theta = 2 * 3.141592653589793 *sqrt (1 - nx * nx - ny * ny - nz * nz);

  // printf ("nx = %f, ny = %f, nz = %f\n", rot[0], rot[1], rot[2]);
  // printf ("nx = %f, ny = %f, nz = %f, theta =%f\n", theta, nx, ny, nz);

  // [[nx*nx nx*ny nx*nz],
  // [ny*nx ny*ny ny*nz],
  // [nz*nx nz*ny nz*nz]]

  T one = ceres::pow (r[0], 0);

  r[0] = ceres::cos (theta) + (one - ceres::cos (theta)) * nx * nx;
  r[1] = -ceres::sin (theta) * nz + (one - ceres::cos (theta)) * nx * ny;
  r[2] = ceres::sin (theta) * ny + (one - ceres::cos (theta)) * nx * nz;
  r[3] = ceres::sin (theta) * nz + (one - ceres::cos (theta)) * ny * nx;
  r[4] = ceres::cos (theta) + (one - ceres::cos (theta)) * ny * ny;
  r[5] = -ceres::sin (theta) * nx + (one - ceres::cos (theta)) * ny * nz;
  r[6] = -ceres::sin (theta) * ny + (one - ceres::cos (theta)) * nz * nx;
  r[7] = ceres::sin (theta) * nx + (one - ceres::cos (theta)) * nz * ny;
  r[8] = ceres::cos (theta) + (one - ceres::cos (theta)) * nz * nz;

  // Eigen::MatrixXd mat (3, 3);
  // mat << r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8];
  // std::cout << mat << std::endl;
}

void get_d_R_d_theta (Eigen::MatrixXd &d_R_d_theta, const double &theta, const double *rv)
{
  const double cos_theta = cos (theta);
  const double sin_theta = sin (theta);

  d_R_d_theta (0, 0) = sin_theta * (pow (rv[0], 2) - 1);
  d_R_d_theta (1, 0) = -rv[2] * cos_theta + rv[0] * rv[1] * sin_theta;
  d_R_d_theta (2, 0) = rv[1] * cos_theta + rv[0] * rv[2] * sin_theta;
  d_R_d_theta (3, 0) = rv[2] * cos_theta + rv[0] * rv[1] * sin_theta;
  d_R_d_theta (4, 0) = -sin_theta + rv[1] * rv[1] * sin_theta;
  d_R_d_theta (5, 0) = -rv[0] * cos_theta + rv[1] * rv[2] * sin_theta;
  d_R_d_theta (6, 0) = -rv[1] * cos_theta + rv[0] * rv[2] * sin_theta;
  d_R_d_theta (7, 0) = rv[0] * cos_theta + rv[1] * rv[2] * sin_theta;
  d_R_d_theta (8, 0) = -sin_theta + rv[2] * rv[2] * sin_theta;
}


void get_d_R_d_nn (Eigen::MatrixXd &d_R_d_nn, const double &theta, const double &nx, const double &ny, const double &nz)
{
  const double cos_theta = cos (theta);
  const double sin_theta = sin (theta);

  d_R_d_nn.block (0, 0, 1, 3) << 2 * nx, 0, 0;
  d_R_d_nn.block (1, 0, 1, 3) << ny, nx, -sin_theta / (1 - cos_theta);
  d_R_d_nn.block (2, 0, 1, 3) << nz, sin_theta / (1 - cos_theta), nx;
  d_R_d_nn.block (3, 0, 1, 3) << ny, nz, sin_theta / (1 - cos_theta);
  d_R_d_nn.block (4, 0, 1, 3) << 0, 2 * ny, 0;
  d_R_d_nn.block (5, 0, 1, 3) << -sin_theta / (1 - cos_theta), nz, ny;
  d_R_d_nn.block (6, 0, 1, 3) << nz, -sin_theta / (1 - cos_theta), nx;
  d_R_d_nn.block (7, 0, 1, 3) << sin_theta / (1 - cos_theta), nz, ny;
  d_R_d_nn.block (8, 0, 1, 3) << 0, 0, 2 * nz;

  d_R_d_nn = (1 - cos (theta)) * d_R_d_nn;
}


void get_d_nn_d_n (Eigen::MatrixXd &d_nn_d_n, const double &theta, const double &nx, const double &ny, const double &nz)
{
  d_nn_d_n.block (0, 0, 1, 3) << 1 - nx *nx, -nx *ny, -nx *nz;
  d_nn_d_n.block (1, 0, 1, 3) << -nx *ny, 1 - ny *ny, -ny *nz;
  d_nn_d_n.block (2, 0, 1, 3) << -nx *nz, -ny *nz, 1 - nz *nz;
  d_nn_d_n = d_nn_d_n / theta;
}

// number of residuals, size of 1st parameter, size of 2nd parameter
template<typename T>
class ResidualMatrix : public ceres::SizedCostFunction <2, 5, 3, 3, 3>
{
public:
  // from ceres-solver/examples/simple_bundle_adjuster.cc
  // ResidualMatrix (const double * const p_observed)
  ResidualMatrix (const double *const p_observed, const double *const addr)
    : p_observed_ (p_observed), addr_ (addr) {}

  ~ResidualMatrix() {}

  //  camera_matrix[9], point in world[3], rotation vector [3], transformation matrix [3], residual [2]
  bool Evaluate (
    double const *const *parameters,
    double *residuals,
    double **jacobians
  ) const override final
  {
    g_cnt++;
    // std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
    // const T *const MC, const T *const P, const T *const rv, const T *const Trans, T *residual
    const double *const MC = parameters[0]; // camera matrix
    const double *const P = parameters[1]; // calibration board coordinate in the world
    const double *const rv = parameters[2]; // rotation vector
    const double *const Trans = parameters[3]; // transformation vector
    // decltype (MC) R[9] = {0}; // Rotation Matrix

    // need to modify, no const allowed
    double R[9]; // Rotation Matrix
    rotation_matrix (R, rv);
    Eigen::Map<const Eigen::Matrix<double, 3, 3, Eigen::RowMajor> > R_mat (R);
    // std::cout << "R = \n" << *R << std::endl;

    // TODO: make pattern distannce a parameter
    //  ideal_point_x           i*va.x           j * vb.x
    auto x = P[0] + 0.027143 * addr_[0]; // x on x axis of world

    //  ideal_point_y           i*va.y           j * vb.y
    auto y = P[1] + 0.027143 * addr_[1]; // y on y axis of world
    auto z = P[2]; // no depth in calibration board


    // append a column vector P to matrix RT
    Eigen::MatrixXd RT_mat (3, 4);
    RT_mat <<  R[0], R[1], R[2], Trans[0],
           R[3], R[4], R[5], Trans[1],
           R[6], R[7], R[8], Trans[2];

    // [R T] * [P]
    // Eigen::Map<const Eigen::Matrix<double, 4, 1> > P_mat_temp (P);
    Eigen::MatrixXd P_mat (4, 1);
    P_mat << x, y, z, 1;

    Eigen::MatrixXd MC_mat (3, 3);
    MC_mat << MC[0], MC[1], MC[2], 0, MC[3], MC[4], 0, 0, 1;
    // std::cout << "MC_mat =\n" << MC_mat << std::endl;

    Eigen::MatrixXd P_mat_estimation (3, 1);
    // camera matrix * transformation matrix * position
    P_mat_estimation = MC_mat * RT_mat * P_mat; // dimension 3x1

    // std::cout << "P_mat_estimation =\n" << P_mat_estimation << std::endl;
    // pm (P_mat_estimation);
    // std::cout << "P_observed = " << p_observed_[0] << ", " << p_observed_[1] << std::endl;

    residuals[0] = P_mat_estimation (0, 0) - p_observed_[0];
    residuals[1] = P_mat_estimation (1, 0) - p_observed_[1];

    // std::cout << "residuals[0] = " << residuals[0] << std::endl;
    // std::cout << "residuals[1] = " << residuals[1] << std::endl;

    // ******************************
    double theta = sqrt (rv[1] * rv[1] + rv[0] * rv[0] + rv[2] * rv[2]);
    // Eigen::MatrixXd <double, 3, 3, Eigen::RowMajor> d_R_d_theta;
    Eigen::MatrixXd d_R_d_theta (9, 1);

    get_d_R_d_theta (d_R_d_theta, theta, rv);

    double nx = rv[0] / theta;
    double ny = rv[1] / theta;
    double nz = rv[2] / theta;

    Eigen::MatrixXd d_theta_d_n (1, 3);
    d_theta_d_n << nx, ny, nz;

    Eigen::MatrixXd d_R_d_nn (9, 3);
    get_d_R_d_nn (d_R_d_nn, theta, nx, ny, nz);

    Eigen::MatrixXd d_nn_d_n (3, 3);
    get_d_nn_d_n (d_nn_d_n, theta, nx, ny, nz);

    Eigen::MatrixXd d_R_d_n (9, 3);
    //  9*3      9*3        3*3          9*1          1*3
    d_R_d_n = d_R_d_nn * d_nn_d_n + d_R_d_theta * d_theta_d_n;

    // jacobian[j][i*parameter_size + k] = d residual[i]/ d parameter[j][k]
    if (jacobians != NULL && jacobians[0] != NULL)
    {
      std::vector <int> empty = {0, 1, 2, 3, 4};
      for (auto i : empty)
      {
        jacobians[0][i] = 0;
        jacobians[0][5 + i] = 0;
      }

      // camera matrix
      // D [x, fx] -> D(x, MC[0])
      jacobians[0][0] = (RT_mat * P_mat) (0, 0);

      // D [x, gamma] -> D(x, MC[1])
      jacobians[0][1] = (RT_mat * P_mat) (1, 0);

      // D [x, cx] -> D(x, MC[2])
      jacobians[0][2] = (RT_mat * P_mat) (2, 0);

      // D (y, fy) -> D (y, MC[4])
      jacobians[0][8] = (RT_mat * P_mat) (1, 0);

      // D (y, cy) -> D (y, MC[5])
      jacobians[0][9] = (RT_mat * P_mat) (2, 0);

      // position vector, 5-7, position in real world
      Eigen::MatrixXd d_P_mat (4, 1);

      d_P_mat << 1.0, 0, 0, 0;
      jacobians[1][0] = (MC_mat * RT_mat * d_P_mat) (0, 0); // residuals[0], D [(Pnew) , (x)]
      jacobians[1][3] = (MC_mat * RT_mat * d_P_mat) (1, 0); // residuals[1], D [(Pnew) , (x)]

      d_P_mat << 0, 1.0, 0, 0;
      jacobians[1][1] = (MC_mat * RT_mat * d_P_mat) (0, 0); // residuals[0], D [(Pnew) , (y)]
      jacobians[1][4] = (MC_mat * RT_mat * d_P_mat) (1, 0); // residuals[1], D [(Pnew) , (y)]

      d_P_mat << 0, 0, 1.0, 0;
      jacobians[1][2] = 0; // residuals[0], D [(Pnew) , (z)]
      jacobians[1][5] = 0; // residuals[1], D [(Pnew) , (z)]

      // rotation vector 8-10
      Eigen::MatrixXd d_R_d_n_row (3, 4);
      d_R_d_n_row << d_R_d_n (0, 0), d_R_d_n (1, 0), d_R_d_n (2, 0), 0,
                  d_R_d_n (3, 0), d_R_d_n (4, 0), d_R_d_n (5, 0), 0,
                  d_R_d_n (6, 0), d_R_d_n (7, 0), d_R_d_n (8, 0), 0;

      jacobians[2][0] = (MC_mat * d_R_d_n_row * P_mat) (0, 0);
      jacobians[2][3] = (MC_mat * d_R_d_n_row * P_mat) (1, 0);

      d_R_d_n_row << d_R_d_n (0, 1), d_R_d_n (1, 1), d_R_d_n (2, 1), 0,
                  d_R_d_n (3, 1), d_R_d_n (4, 1), d_R_d_n (5, 1), 0,
                  d_R_d_n (6, 1), d_R_d_n (7, 1), d_R_d_n (8, 1), 0;

      jacobians[2][1] = (MC_mat * d_R_d_n_row * P_mat) (0, 0);
      jacobians[2][4] = (MC_mat * d_R_d_n_row * P_mat) (1, 0);

      d_R_d_n_row << d_R_d_n (0, 2), d_R_d_n (1, 2), d_R_d_n (2, 2), 0,
                  d_R_d_n (3, 2), d_R_d_n (4, 2), d_R_d_n (5, 2), 0,
                  d_R_d_n (6, 2), d_R_d_n (7, 2), d_R_d_n (8, 2), 0;

      jacobians[2][2] = (MC_mat * d_R_d_n_row * P_mat) (0, 0);
      jacobians[2][5] = (MC_mat * d_R_d_n_row * P_mat) (1, 0);

      // transformation vector 11-13
      Eigen::MatrixXd d_P (4, 1);
      d_P << 1, 0, 0, 0;
      jacobians[3][0] = (MC_mat * RT_mat * d_P) (0, 0);
      jacobians[3][3] = (MC_mat * RT_mat * d_P) (1, 0);

      d_P << 0, 1, 0, 0;
      jacobians[3][1] = (MC_mat * RT_mat * d_P) (0, 0);
      jacobians[3][4] = (MC_mat * RT_mat * d_P) (1, 0);

      d_P << 0, 0, 1, 0;
      jacobians[3][2] = (MC_mat * RT_mat * d_P) (0, 0);
      jacobians[3][5] = (MC_mat * RT_mat * d_P) (1, 0);
    }
    // std::cout << "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv" << std::endl;
    return true;
  }

private:
  const double *const p_observed_;
  const double *const addr_;
};

void prepare_random()
{
  struct timeval time;
  gettimeofday (&time, NULL);
  long micro_seconds = (time.tv_sec * 1000000) + (time.tv_usec);
  srand (micro_seconds);
}

int main (int argc, char **argv)
{
  printf ("argc = %d\n", argc);

  double input[3] = {0.1, 0.2, 0.3};
  double output[9] = {0.0};
  rotation_matrix (output, input);
  // *************************************************************************
  google::InitGoogleLogging (argv[0]);

  std::string filename = "data.txt";

  FILE *fptr = fopen (filename.c_str(), "r");
  int num_cameras, num_points, num_observations;

  FscanfOrDie (fptr, "%d", &num_cameras);
  FscanfOrDie (fptr, "%d", &num_points);
  FscanfOrDie (fptr, "%d", &num_observations);

  printf ("num_cameras = %d, num_points = %d, num_observations = %d\n", num_cameras, num_points, num_observations);

  //                 1            35             9       * 2 coordinate per point
  ssize_t len = num_cameras * num_points * num_observations * 4;
  auto data = new double[len];
  double *data_p = &data[0];

  read_calibration_to_array (fptr, data_p, len);

  std::cout << "num_cameras * num_points * num_observations * 4 =" << len << std::endl;

  double camera_parameters[5] = {1.1, 0.01, 1.1, 1.1, 1.1};
  double trans[3];

  prepare_random();
  for (int i = 0; i < 3; i++)
  {
    trans[i] = exp ((float)rand() / ((float)RAND_MAX) * 3);
  }
  // double m_dot_in_image[3] = {0.0};
  // double m_dots_after_calibrate[3] = {0.0};

  double *p_origin = new double[num_observations * 3];
  double *rot =  new double[num_observations * 3];

  // 0.027143

  for (int i = 0; i < num_observations; i++)
  {
    p_origin[3 * i] = 0.5 + 0.01 * i; // 0
    p_origin[3 * i + 1] = -0.5 + 0.01 * i; // 0
    p_origin[3 * i + 2] = 0;
    rot[3 * i] = 1 + 0.01 * i;
    rot[3 * i + 1] = 1 + 0.01 * i;
    rot[3 * i + 2] = 1 + 0.01 * i;
  }

  Problem problem;
  for (int j = 0; j < num_observations; ++j)
  {
    for (int i = 0; i < num_points; ++i)
    {
      auto p = data_p + 4 * i + 4 * j * num_points;
      printf ("%d, %d, %lf, %lf, %lf, %lf\n", j, i, * (p + 2), * (p + 3), * (p), * (p + 1));
      ceres::CostFunction *cost_function =
        // ResidualMatrix::Create (data_p + (4 * i + 2), data_p + (4 * i));

        // 2 is the number of residuals
        // 9 means there are 9 parameters in camera matrix
        // 3 is dimension of p_origin
        // 3 is the elements of rotation vector
        // 3 is the elements of transformation vector

        // distrotion is ignored
        // point_index, observed_position
        new ResidualMatrix<const double *const> (p + 2, p);

      // double *index =
      problem.AddResidualBlock (
        cost_function,
        NULL /* squared loss */,
        camera_parameters, p_origin + j * 3, rot + j * 3, trans);
    }
  }


  Solver::Options options;
  options.max_num_iterations = 100;
  options.linear_solver_type = ceres::DENSE_QR;
  options.minimizer_progress_to_stdout = true;
  Solver::Summary summary;
  Solve (options, &problem, &summary);
  std::cout << summary.BriefReport() << "\n";

  Eigen::MatrixXd mat_camera (3, 3);
  auto c = camera_parameters;
  mat_camera << c[0], c[1], c[2], 0, c[3], c[4], 0, 0, 1;

  std::cout << "camera_parameters =\n" << mat_camera;
  std::cout << std::endl;
  printf ("trans = %f, %f, %f\n", trans[0], trans[1], trans[2]);

  for (int j = 0; j < num_observations; ++j)
  {
    printf ("j = %d\n", j);
    printf ("    p_origin = %f, %f, %f\n", p_origin[0 + 3 * j], p_origin[1 + 3 * j], p_origin[2 + 3 * j]);
    printf ("    rot = %f, %f, %f\n", rot[0 + 3 * j], rot[1 + 3 * j], rot[2 + 3 * j]);
  }

  std::cout << summary.BriefReport() << std::endl;
  // std::cout << "************************" << std::endl;
  // std::cout << summary.FullReport() << std::endl;

  delete [] data;
  delete [] p_origin;
  delete [] rot;

  return 0;
}
